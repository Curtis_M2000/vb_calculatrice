﻿Public Class Form1
    Dim firstnum, secondnum, answer As Double
    Dim opera As String

    Private Sub Button19_Click(sender As Object, e As EventArgs) Handles Button19.Click
        secondnum = Label1.Text
        If opera = "+" Then
            answer = firstnum + secondnum
            Label1.Text = answer
            Label2.Text = ""
        ElseIf opera = "-" Then
            answer = firstnum - secondnum
            Label1.Text = answer
            Label2.Text = ""
        ElseIf opera = "*" Then
            answer = firstnum * secondnum
            Label1.Text = answer
            Label2.Text = ""
        ElseIf opera = "/" Then
            answer = firstnum / secondnum
            Label1.Text = answer
            Label2.Text = ""
        ElseIf opera = "mod" Then
            answer = firstnum Mod secondnum
            Label1.Text = answer
            Label2.Text = ""
        ElseIf opera = "Exp" Then
            answer = firstnum ^ secondnum
            Label1.Text = answer
            Label2.Text = ""
        End If
    End Sub

    Private Sub Button18_Click(sender As Object, e As EventArgs) Handles Button18.Click
        If Label1.Text.Length > 0 Then
            Label1.Text = Label1.Text.Remove(Label1.Text.Length - 1, 1)
        End If
    End Sub

    Private Sub Button17_Click(sender As Object, e As EventArgs) Handles Button17.Click
        Label1.Text = "0"
        Label2.Text = ""
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If (Label1.Text.Contains("0")) Then
            Label1.Text = Label1.Text.Remove(0, 1)
        End If
    End Sub

    Private Sub button_click(sender As Object, e As EventArgs) Handles Button9.Click, Button8.Click, Button7.Click, Button6.Click, Button5.Click, Button4.Click, Button12.Click, Button11.Click, Button10.Click, Button2.Click, Button1.Click
        Dim b As Button = sender
        If Label1.Text = "0" Then
            Label1.Text = b.Text
        Else
            Label1.Text = Label1.Text + b.Text
        End If
    End Sub

    Private Sub operation_arithmetique(sender As Object, e As EventArgs) Handles Button16.Click, Button15.Click, Button14.Click, Button13.Click
        Dim ops As Button = sender
        firstnum = Label1.Text
        Label2.Text = Label1.Text
        Label1.Text = ""
        opera = ops.Text
        Label2.Text = Label2.Text + "" + opera
    End Sub
End Class
